<?php

namespace robinrosiers\SerialPort;

use robinrosiers\SerialPort\Configure\ConfigureInterface;
use robinrosiers\SerialPort\Configure\TTYConfigure;
use robinrosiers\SerialPort\Exception\DeviceNotAvailable;
use robinrosiers\SerialPort\Exception\DeviceNotFound;
use robinrosiers\SerialPort\Exception\DeviceNotOpened;
use robinrosiers\SerialPort\Exception\WriteNotAllowed;
use robinrosiers\SerialPort\Parser\ParserInterface;
use robinrosiers\SerialPort\Parser\SeparatorParser;

/**
 * SerialPort to handle serial connection easily with PHP
 * Suitable for Arduino communication or Qibixx MDB devices :)
 *
 * @copyright MIT
 */
class SerialPort
{
    /**
     * File descriptor
     *
     * @var resource
     */
    private $fd = false;

    /**
     * @var ParserInterface
     */
    private $parser;

    /**
     * @var ConfigureInterface
     */
    private $configure;

    /**
     * @param ParserInterface|null    $parser
     * @param ConfigureInterface|null $configure
     */
    public function __construct(ParserInterface $parser = null, ConfigureInterface $configure = null)
    {
        $this->parser = $parser;
        $this->configure = $configure;
    }

    /**
     * Open serial connection
     *
     * @param string $device path to device
     * @param string $mode fopen mode
     *
     * @return bool
     *
     * @throws DeviceNotAvailable|DeviceNotFound
     */
    public function open($device, $mode = "w+b")
    {
        if (false === file_exists($device)) {
            throw new DeviceNotFound();
        }

        $this->getConfigure()->configure($device);
        $this->fd = fopen($device, $mode);

        if (false !== $this->fd) {
            stream_set_blocking($this->fd, false);

            return true;
        }

        unset($this->fd);
        throw new DeviceNotAvailable($device);
    }

    /**
     * Write data into serial port line
     *
     * @param string $data
     *
     * @return int length of byte written
     *
     * @throws WriteNotAllowed|DeviceNotOpened
     */
    public function write($data)
    {
        $this->ensureDeviceOpen();

        $dataWritten = fwrite($this->fd, $data);
        if (false !== $dataWritten) {
            fflush($this->fd);

            return $dataWritten;
        }

        throw new WriteNotAllowed();
    }

    /**
     * Read data byte per byte until separator found or a timeout is reached
     *
     * @param int $maxElapsed infinite
     * @return string
     */
    public function read($maxElapsed = 'infinite')
    {
        $this->ensureDeviceOpen();
    
        $chars = [];
        $timeout = $maxElapsed === 'infinite' ? 1.7976931348623E+308 : (microtime(true) + $maxElapsed);
        do {
            $char = fread($this->fd, 1);
            if ($char === '') {
                if (microtime(true) > $timeout) return false;
                usleep(100);    //Why waste CPU?
                continue;
            }
            $chars[] = $char;
        } while ($char !== $this->getParser()->getSeparator());

        return $this->getParser()->parse($chars);
    }

    /**
     * Close serial connection
     *
     * @return bool return true on success
     *
     * @throws DeviceNotOpened
     */
    public function close()
    {
        $this->ensureDeviceOpen();

        $hasCloseFd = fclose($this->fd);
        $this->fd = false;

        return $hasCloseFd;
    }

    /**
     * configure serial line
     *
     * @return ConfigureInterface
     */
    private function getConfigure()
    {
        if (null === $this->configure) {
            $this->configure = new TTYConfigure();
        }

        return $this->configure;
    }

    /**
     * Get parser, if not defined, return new line parser by default
     *
     * @return ParserInterface
     */
    private function getParser()
    {
        if (null === $this->parser) {
            $this->parser = new SeparatorParser();
        }

        return $this->parser;
    }

    /**
     * @throws DeviceNotOpened
     */
    private function ensureDeviceOpen()
    {
        if (!$this->fd) {
            throw new DeviceNotOpened();
        }
    }
}
