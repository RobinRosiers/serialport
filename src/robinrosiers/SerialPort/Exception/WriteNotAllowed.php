<?php

namespace robinrosiers\SerialPort\Exception;

class WriteNotAllowed extends RuntimeException
{
    protected $message = "Write not allowed on device. Please check fopen mode.";
}
